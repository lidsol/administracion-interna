# Reunion del equipo de LIDSOL  23 de Sep 2019  
*Proxima reunion viernes 27 de Sep de 2019 a las 12 p.m. en LIDSOL*

## Gestión
* ¿Qué roles si se asignará?
* Definición de las necesidades del laboratorio
* Taller de modificación del sitio
* Redacción y desarrollo de temas
  * Levantar issues para proponer temas
    * Fase 1: Aprobación
    * Fase 2: Redacción y merge request
    * Fase 3: Revisión
  * Portar las publicaciones regadas de lidsol
* **Inventario** (*Urgente para el miercoles 25 de Septiebre 2019*)

## Repos
* Guia de contribución
  * Guia de estilo
    * Commits
    * Comentarios entre lineas
  * Flujo de revisión
* Limpieza 

## Sitio
* Taller de como contriur el sitio
  * Como funciona hugo
  * Como funciona el tema
* Flujo para agregar contenido 

## Redes sociales
* Roles
* Flujo
* Calendario

## Laboratorio
* **CoC / Convivencia** (*Por comentar con todo el equipo*)
 * Leer el CoC 
 * Si hay dudas o inconformidades hacerlas llegar
* Reglamento interno
* Padawans
* Grupos de estudios
* CCOSS
  * Gnome (curso pendiente)
  * cpython (Python Day 2019 Octubre)
  * Tensorflow (por definir)
  * Firefox (pro definir)
* UPIITA

## Proyectos
* **Ángeles verdes**
  * Vamos atrasados 
  * ¿Cómo ayuda el resto del lab? (Para el 25 de Septiembre)
    * Necesitamos un DevOps
    * Redacción de la parte técnica
    * Justificación de tecnologías (Django, VUE.js, MariaDB, Kotlin)
      * ¿Qué tipo de tecnología es?
      * ¿Porqué es buena?
      * Diferencia de tecnologías y ventajas sobre otras
      * ¿Quiénes la están utilizando?
      * Media cuartilla máximo
      * Diego justifica Django + python
  * Involucrados
    * Emilio
    * Paul
    * Alexis
    * Luis
    * Pablo
    * Contactar con el Desarrollador Web que consiguió Pablo
  * Delimitar los roles y que autoridad tiene cada integrante para asumir dicho rol 
  * Cotizaciones
* Mecanismo de privacidad y anonimato
* Podcast para explicar que es el software libre, como contribuir, como acercarse a cargo de Luz
  * Ya está una estructura
  * Falta grabarlo
  * A donde se va a subir
