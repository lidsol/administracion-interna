---
title: Reporte 2019
author: LIDSoL
date: 15 de Noviembre de 2019
---

# Descripción del LIDSoL

El Laboratorio de Investigación y Desarrollo de Software Libre (LIDSoL) forma
parte de los laboratorios de investigación del Departamento de Computación de
la División de Ingeniería Eléctrica de la Facultad de Ingeniería de la UNAM.

Fue establecido en el año 2001 por voluntarios, alumnos, ex-alumnos y académicos
interesados en el estudio, discusión, desarrollo e investigación de tecnologías
libres.

El objetivo del laboratorio es promover el uso e impulsar la investigación y el
desarrollo de tecnologías libres, realizando proyectos en distintas áreas
buscando resolver problemas que tengan un impacto positivo en la comunidad
universitaria y la sociedad.

En el laboratorio se realizan diversas actividades, algunas temporales y
otras permanentes, entre las que destacan:

* Cursos
* Talleres
* Conferencias
* Asesorías en el uso, instalación, desarrollo y licenciamiento de software
libre
* Desarrollo de proyectos de investigación
* Desarrollo y contribución a proyectos de software libre

Todo se realiza empleando tecnologías que siguen los principios de libertad
a las que está alineado el laboratorio.

El LIDSoL es también el hogar del Capítulo Estudiantil de la ACM UNAM-FI.

## Ubicación

El laboratorio se encuentra ubicado en el Conjunto Sur (Anexo) de la Facultad
de Ingeniería, en el Edificio P División de Ingeniería Eléctrica, Piso 2, salón
P-213.

## Contacto

El laboratorio cuenta con varios medios de comunicación para mantenerse en
contacto con integrantes y ex-integrantes del laboratorio así como con otras
comunidades de software libre del país.

* Correo: [lidsol@protonmail.com](mailto:lidsol@protonmail.com)
* Lista de correo: lidsol@softwarelibre.mx
  * Suscripción: <https://lists.srvr.mx/mailman/listinfo/lidsol>
* Twitter: [lidsol](https://twitter.com/lidsol)
* Facebook: <https://facebook.com/lidsol.unam>
* Telegram: <https://telegram.me/lidsol>
* Sitio web: <https://lidsol.org>

# Inventario

A continuación se enlista el mobiliario y equipo de cómputo con el que cuenta el
laboratorio, algunos equipos que están en el laboratorio por colaboración con
otros proyectos y sólo están en el laboratorio durante la duración del proyecto.

## Equipos

### Laptops y Computadoras

| Cantidad | Artículo             | Inventario | Estado    | Procedencia                  |
| -------- |----------------------|------------|-----------|------------------------------|
| 1        | Dell Inspiron 560s   | 02318452   | Funcional | LIDSoL                       |
| 1        | Dell Inspiron 560s   | 02318451   | Funcional | LIDSoL                       |
| 1        | Aspire VC5-591G      | 02495850   | Funcional | LIDSoL                       |
| 1        | XPS 8910             | 02488874   | Funcional | Colaboración PAPIME PE104416 |
| 1        | Dell Dimension 5150  | 02226268   | Funcional | LIDSoL                       |
| 1        | HP Compaq DC5850     | 02292359   | Funcional | LIDSoL                       |
| 1        | Dell Powerdge SC1430 | 02242125   | Funcional | Desconocido                  |

### Monitores y Pantallas

| Cantidad | Artículo                 | Inventario | Estado    | Procedencia                  |
| -------- |--------------------------|------------|-----------|------------------------------|
| 6        | Monitore HP L1710        | -          | Funcional | LIDSoL                       |
| 1        | Monitor HP 2511t         | -          | Funcional | LIDSoL                       |
| 1        | Pantalla LED LG 32lf550b | 15249      | Funcional | LIDSoL                       |
| 1        | Monitor Dell in1020nb    | 02318454   | Funcional | LIDSoL                       |
| 1        | Monitor Dell in1020nb    | 02318453   | Funcional | LIDSoL                       |
| 2        | Monitor Dell se2417hg    | -          | Funcional | Colaboración PAPIME PE104416 |


### Periféricos

| Cantidad | Artículo                     | Inventario | Estado    | Procedencia                  |
| -------- |------------------------------|------------|-----------|------------------------------|
| 2        | Teclado Dell sk-8185         | -          | Funcional | LIDSoL                       |
| 2        | Mouse Dell ms111p            | -          | Funcional | LIDSoL                       |
| 1        | Mouse Microsoft              | -          | Funcional | Diego Barriga                |
| 1        | Bocinas Logitech z313        | -          | Funcional | LIDSoL                       |
| 1        | Audifonos Sennnheiser        | -          | Funcional | LIDSoL                       |
| 1        | Audifonos Power-Beats        | -          | Funcional | LIDSoL                       |
| 1        | No break ISB                 | -          | Funcional | LIDSoL                       |
| 1        | Impresora HP p1102w          | -          | Funcional | LIDSoL                       |
| 1        | Mouse y teclado Dell 580addv | -          | Funcional | Colaboración PAPIME PE104416 |
| 1        | Control Xbox 360             | -          | Funcional | Colaboración PAPIME PE104416 |
| 1        | Control Xbox One             | -          | Funcional | Colaboración PAPIME PE104416 |
| 1        | Oculus Rift                  | 2491027    | Funcional | Colaboración PAPIME PE104416 |
| 1        | LEAP Motion                  | -          | Funcional | Colaboración PAPIME PE104416 |
| 1        | Visor Marvel CardBoard       | -          | Funcional | Colaboración PAPIME PE104416 |
| 1        | Visor Samsung Gear VR        | -          | Funcional | Colaboración PAPIME PE104416 |

### Redes

| Cantidad | Artículo                         | Inventario | Estado      | Procedencia |
| -------- |----------------------------------|------------|-------------|-------------|
| 1        | Switch Zonet zfs3016             | -          | Funcional   | LIDSoL      |
| 2        | T. Inalámbrica TPLink tl-wn781dn | -          | Funcional   | LIDSoL      |
| 2        | T. Inalámbrica TPLink tl-wn751nd | -          | Funcional   | LIDSoL      |
| 1        | Transmisor FM                    | 15317      | Funcional   | LIDSoL      |
| 1        | Router Engenius csr600           | -          | Funcional   | LIDSoL      |
| 1        | Router Linxys ea2700             | -          | Funcional   | LIDSoL      |

### Mobiliario

| Cantidad | Artículo                | Estado    | Procedencia |
| -------- |-------------------------|-----------|-------------|
| 11       | Silla con ruedas        | Funcional | LIDSoL      |
| 1        | Banco de madera         | Funcional | LIDSoL      |
| 5        | Mesas de trabajo        | Funcional | LIDSoL      |
| 1        | Pizarrón blanco         | Funcional | LIDSoL      |
| 1        | Refigerador             | Funcional | LIDSoL      |
| 1        | Horno de microondas     | Funcional | LIDSoL      |
| 1        | Cafetera                | Funcional | LIDSoL      |
| 1        | Estante gris            | Funcional | LIDSoL      |
| 1        | Aire acondicionado      | Funcional | LIDSoL      |
| 1        | Mueble de madera        | Funcional | LIDSoL      |
| 1        | Garrafón de Agua 10 Lts | Funcional | LIDSoL      |

### Otros

| Cantidad | Artículo                     | Inventario | Estado    | Procedencia |
| -------- |------------------------------|------------|-----------|-------------|
| 1        | Extintor                     | -          | Funcional | LIDSoL      |
| 3        | Señalamimentos de emergencia | -          | Funcional | LIDSoL      |

# Miembros

El laboratorio esta compuesto por alumnos, académicos y exalumnos que participan
de forma presencial y a distancia en el laboratorio. A continuación se hace un
listado únicamente de aquellos colaboradores que son alumnos inscritos
o académicos que laboran en CU y asisten de forma presencial al laboratorio con
regularidad.

## Gunnar Eyal Wolf Iszaevich

* Técnico Académico Titular B de Tiempo Completo adscrito a la Secretaría
Técnica del Instituto de Investigaciones Económicas, UNAM
* Responsable académico del laboratorio
* Responsable académico del Capítulo de la ACM UNAM-FI
* Dirige el proyecto [PAPIME PE102718](#pe102718)
* Correo: [gwolf@iiec.unam.mx](mailto:gwolf@iiec.unam.mx)
* Telefono:
  * Extension UNAM: 30154
  * Celular: 5514512244

## Andrés Hernández

* Titulado de Ingeniería en Computación
* Profesor en la Facultad de Ciencias
* Profesor en el CERT UNAM
* Gestiona de los registros DNS del dominio <https://lidsol.org>

## Alexis Manuel Ríos Morales

* Estudiante de Ingeniería en Computación
* Número de cuenta: 311321319

## Cinthya Celina Tamayo González

* Estudiante de Ingeniería en Telecomunicaciones
* Número de cuenta: 313176225
* Becaria del proyecto [PAPIME PE102718](#pe102718)
* Prestadora de servicio social

## Diego Alberto Barriga Martínez

* Pasante de Ingeniería en Computación
* Número de cuenta: 311033607
* Tesorero del Capítulo de la ACM UNAM-FI
* Tesista: [Procesamiento de Lenguaje Natural para lenguas de bajos recursos]
* Colaborador del proyecto [PAPIME PE102718](#pe102718)
* Colaborador de Tsunkua

## Juan Antonio Flores Gaspar

* Estudiante de Ingeniería en Computación
* Número de cuenta: 312030661
* Becario del proyecto [PAPIME PE102718](#pe102718)

## Luis Alberto Oropeza Vilchis

* Estudiante de Ingeniería en Computación
* Número de cuenta: 311211931
* Becario del proyecto [PAPIIT IT101917](#it101917)
* Administrador del servicio Nextcloud del laboratorio

## Luz Olympia Torres Rosales

* Estudiante de Ingeniería Eléctrica
* Número de cuenta: 310225056
* Encargada de difusión de eventos y redes sociales

## Oscar Emilio Cabrera López

* Estudiante de Ingeniería en Computación
* Número de cuenta: 31233261
* Presidente del Capítulo de la ACM UNAM-FI
* Colaborador del proyecto [PAPIME PE102718](#pe102718)
* Administrador de la red y los servidores del laboratorio
* Correo: [emilio1625@gmail.com](mailto:emilio1625@gmail.com)
* Teléfono: 5534649678

## Pablo Vivar Colina

* Estudiante de Ingeniería Eléctrica
* Número de cuenta: 310738134

## Paul Sebastian Aguilar Enriquez

* Estudiante de Ingeniería en Computación
* Número de cuenta: 415028130
* Colaborador del proyecto [PAPIME PE102718](#pe102718)
* Colaborador de Tsunkua
* Correo: [paul.aguilar.enriquez@hotmail.com](mailto:paul.aguilar.enriquez@hotmail.com)
* Teléfono: 3312438124

# Actividades

En el laboratorio se realizan múltiples actividades, entre ellas charlas,
talleres y ponencias de temas relacionados con el software libre, su desarrollo,
su impacto tecnológico y su impacto en la sociedad. A continuación se enlistan
las actividades que se realizaron o se están realizando en el transcurso de
2019.

## Proyectos activos

### PAPIME PE102718 Desarrollo de materiales didácticos para los mecanismos de privacidad y anonimato { #pe102718 }

#### Objetivos:

* Concientización acerca de las expectativas razonables de privacidad en línea
* Fomentar la adopción de herramientas que permitan el uso seguro y anónimo de
las redes de comunicaciones, particularmente de la red Tor
* Generar material que ayude a la comprensión de la base tecnológica sobre la
cual se fundamentan TOR y otras tecnologías anonimizadoras
* La instalación en el laboratorio de sistemas que, mediante distintas
estrategias, permitan el uso, evaluación y monitoreo de recursos de los
distintos mecanismos de privacidad y anonimato en redes

La descripción completa del proyecto se puede encontrar en
<https://lidsol.org/project/priv-anon/>.

#### Miembros del proyecto:

* Responsable: [Gunnar Eyal Wolf Iszaevich]
* Miembros del laboratorio participando:
  * [Cinthya Celina Tamayo González]
  * [Juan Antonio Flores Gaspar]
  * [Oscar Emilio Cabrera López]
  * [Diego Alberto Barriga Martínez]

#### Actividades

* Transcripción de ponencias de coloquios pasados
* Producción de material audiovisual de difusión sobre el funcionamiento de
redes anonimizadoras
* Impartición de talleres sobre la importancia y el uso de redes anonimizadoras
* Desarrollo de un proxy anonimizador de TOR
* Libro sobre el coloquio *Mecanismos de privacidad y anonimato en redes: La red TOR*
* Administración de un nodo intermedio de la red TOR

#### Actividades concluidas:

* Coloquio Mecanismos de Privacidad y Anonimato en Redes
* Curso Privacidad y anonimato para un manejo seguro de mi información en redes
* Instalación de un nodo intermedio de la Red TOR

### Corpus paralelo Tsunkua

#### Descripción

Tsunkua es un corpus paralelo español-otomí que permite búsquedas de palabras
o frases dentro de una colección de documentos bilingües digitalizados
(traducciones). Este tipo de sistemas son útiles para estudiosos, aprendices
y hablantes de otomí (hñahñu) que quieran observar cómo se traduce cierta
palabra o frase dependiendo del contexto o de la fuente.

#### Objetivos

* Desarrollo de una plataforma de búsqueda de frases en español u otomí
* Facilitar el acceso a un corpus paralelo español-otomí a lingüistas, estudiantes e investigadores de procesamiento de lenguaje natural.

#### Miembros del proyecto

*  [Diego Alberto Barriga Martínez]
*  [Paul Sebastian Aguilar Enriquez]

#### Actividades

* Desarrollo de una plataforma WEB en Django
* Gestión de un motor de búsqueda
* Asesoramiento para la apertura del código fuente del sistema


## Proyectos finalizados

### Portia (Parte del PAPIME PE104416 Ambientes Virtuales y Herramientas Digitales para Neurociencias) { #pe104416 }

#### Miembros del proyecto:

* Responsable: Dr. Rodrigo Montúfar-Chaveznava
* Miembros del laboratorio que participaron:
  * [Luis Alberto Oropeza Vilchis]
  * [Oscar Emilio Cabrera López]
  * [Paul Sebastian Aguilar Enriquez]

#### Objetivos Alcanzados:

* Aplicación de Realidad Virtual para la plataforma Android
* Aplicación de Realidad Virtual para la plataforma Windows
* Artículo
    * Nombre: Exploring Virtual Reality for Neural Rehabilitation
    and Phobia Treatment
    * Libro: New Technologies to Improve Patient Rehabilitation
    * Workshop: International Workshop on ICTs for Improving
    Patients Rehabilitation Research Techniques

## Proyectos en planeación

### Padawans

#### Descripción

El proyecto se piensa como un programa de formación de alumnos de primeros
semestres en metodologías de desarrollo de software colaborativo, que les
permitan colaborar o desarrollar proyectos de software libre acordes a alguna
área de su interés.

El programa consiste en que los alumnos "adopten" un programa de software libre
de su interés y aprendan a colaborar y contribuir con código al proyecto.
También se planteará a los alumnos la posibilidad de desarrollar un proyecto de
software con fines educativos para la Faculta de Ingeniería.

Uno de los objetivos del programa es que al verano siguiente los participantes
puedan participar en el [Google Summer
of Code](https://summerofcode.withgoogle.com) y compartan y asesoren a nuevos
participantes en ediciones posteriores.

#### Objetivos

* Formar a los alumnos en metodologías modernas de desarrollo de software
* Enseñar a los alumnos como colaborar en equipos multidiciplinarios de colaboración remota
* Pasar de una cultura de uso de software libre a una cultura de desarrollo de software libre
* Que cada año los alumnos participen en el Google Summer of Code

### Desarrollo de plataforma tecnológica (hardware y software) escalable para el centro de gestión operativa nacional de la corporación ángeles verdes

#### Descripción

El LIDSoL, junto con el LIESE y el Laboratorio de iOS participa en el diseño de
una plataforma para la corporación Ángeles Verdes en el marco de Convocatoria
del Fondo Sectorial para la Investigación, el Desarrollo y la Innovación
Tecnológica en Turismo SECTUR – CONACYT 2019.

#### Objetivos

* Diseñar con una plataforma tecnológica escalable y con una vigencia
tecnológica de al menos 5 años para el Centro de Gestión Operativa Nacional de
la Corporación Ángeles Verdes, desarrollada usando software libre.
* Formar alumnos en el diseño e implementación de sistemas usando las prácticas
de desarrollo comunes en el software libre.
* Adquirir equipo de cómputo que permita al laboratorio el desarrollo de este
y futuros proyectos.

### Convenio de Comunidad con Linux Professional Institute

#### Descripción

Los representantes del Linux Professional Institute se han acercado al LIDSoL
para ofrecer un acuerdo de comunidad. Este acuerdo le proporcionaría a los
miembros del laboratorio descuentos en cursos de preparación para
certificaciones, capacitación a sus integrantes para impartir cursos de
preparación, descuentos en certificaciones y acceso gratuito a material generado
por el LPI. El laboratorio esta evaluando el acuerdo.

### Podcast LIDSoL

#### Descripción

El podcast busca actualizar a la comunidad acerca de las noticias dentro del SL,
dar difusión a los eventos, comunidades y cursos que se imparten en el Sl dentro
y fuera de la Facultad de Ingeniería. Formar una comunidad de diversas carreras
de ingnería donde se emplea el SL y como éste ayuda a la formación de nuevo
conocimiento y desarrollo de tecnologías. Tener una guía auditiva sobre como se
puede contribuir con el el Laboratio de Investigación y Desarrollo de Software
Libre (LIDSOL) y formar parte de los proyectos que se desarrollan dentro del
laboratorio

#### Objetivos

* Difundir dentro de la comunidad de la Facultad de Ingeniería el mundo del SL,
  para uso de alumnos como de profesores, de una manera sencilla y muy amenana.
* Definir que es SL, en que se usa, por que se usa y los ejemplos más conocidos
  dando nombres y principalmente para que es cada uno o por que se usa.
* Dar información sobre las diferentes distribuciones del SL, sus
  características, sus diferencias y las ventajas de cada una de ellas.

## Actividades continuas

### Administración de los 2 servidores del laboratorio

#### Descripción

El laboratorio cuenta con 2 servidores y 2 dominios en los que aloja servicios
como Nextcloud, un Relay de la red TOR y una NAT que provee de internet al
laboratorio. Los servidores usan sistemas operativos libres y su principal
propósito es la evaluación de tecnologías de despliegue de sistemas WEB
y proveer de servicios necesarios para otros proyectos del laboratorio. Los
servidores son administrados por los alumnos con asesoramiento de los
profesores [Andrés Hernández] y [Gunnar Eyal Wolf Iszaevich], siguiendo
estrictas prácticas de seguridad.

### Administración del servicio Nextcloud

#### Descripción

El servicio de Nexcloud provee al laboratorio de almacenamiento en una "nube"
propia del laboratorio. Esto permite al laboratorio no depender de alternativas
propietarias como Google Drive para la colaboración remota y organización de
proyectos y permite a los alumnos aprender tecnologías de despliegue
y actualización de servicios WEB mediante contenedores, redes de computadoras
y buenas practicas de seguridad en la red.

### Creación de contenido en la pagina web del LIDSoL

#### Descripción

El laboratorio cuenta con un sitio web en la dirección <https://lidsol.org>, los
alumnos mantienen la página actualizada con nuevo contenido, actualización de
los próximos eventos del laboratorio y entradas en el blog del sitio.

### Gestión de las redes sociales del laboratorio

El laboratorio cuenta con una cuenta de Facebook y una de Twitter, en las cuales
se comparte contenido relacionado con el software libre y hace difusión de
cursos, eventos, conferencias y actividades organizadas por el laboratorio o en
las que participa.

### Asesoría a la comunidad FI

#### Descripción

Se atiende las dudas del alumnado de la Facultad de Ingeniería y en general de
cualquier persona que se acerque al laboratorio. Se han instalado sistemas
operativos, resuelto dudas sobre lenguajes de programación, hardware a utilizar
para proyectos personales y de materias cursadas, entre otras. Además, se han
prestado componentes electrónicos diversos.

## Tesis

### Procesamiento de Lenguaje Natural para lenguas de bajos recursos

* Tesista: Diego Alberto Barriga Martinez
* Nombre de la Tesis: Etiquetador automático de la morfología del otomí usando
predicción estructurada
* Id del Tema: 20201035
* Objetivo: Diseñar e implementar un etiquetador morfológico para el otomí
basado en técnicas de Procesamiento del Lenguaje Natural y Aprendizaje de
Máquina. En particular, se hará énfasis en métodos de aprendizaje
estructurado débilmente supervisados. Específicamente, se aplicarán
Conditional Random Fields (CRF) para etiquetado morfológico (glosado) del
otomí, una lengua de bajos recursos.
* Asesor: Mtro. Víctor Mijangos

## Cursos Impartidos

### Introducción a la privacidad y el anonimato digital

- Ponente: [Diego Alberto Barriga Martínez]
- Fecha: 14 al 18 de Enero de 2019
- Lugar: Facultad de Ingeniería, UNAM
- Descripción: Curso enfocado a público con conocimientos básicos de
computación y navegación WEB, con interés en cuidar su privacidad y anonimato.
Se presentan conceptos elementales, herramientas y una visión general del
porqué es importante cuidar nuestra identidad digital
* Cantidad de asistentes: 10 personas

### Introducción a GNU/Linux

- Ponente: [Oscar Emilio Cabrera López]
- Fecha: 21 al 25 de Enero de 2019
- Lugar: Facultad de Ingeniería, UNAM
- Descripción: Curso introductorio al uso de sistemas operativos basados en
GNU/Linux, enfocado a personas que han tenido poco o ningún contactos con
estos sistemas previamente
* Cantidad de asistentes: 9 personas

### Métodos generales en aprendizaje máquina

- Ponentes:
    * Mtro. Victor Mijangos
    * Dra. Ximena Gutiérrez-Vasques
- Fecha: 21 al 25 de Enero de 2019
- Lugar: Facultad de Ingeniería, UNAM
- Descripción: El alumno comprenderá los conceptos teóricos básicos para
entender los métodos principales de aprendizaje de máquina; así como las
herramientas necesarias para su implementación en diferentes tareas,
principalmente de Procesamiento del Lenguaje Natural (PLN).
* Cantidad de asistentes: 12 personas

### Criptografía

- Ponente: Dra. Gina Gallegos García
- Fecha: 14 al 18 de Enero de 2019
- Lugar: Facultad de Ingeniería, UNAM
- Descripción: Curso introductorio a la criptografía, abarcando desarrollo
histórico, fundamentos matemáticos, y algunas aplicaciones de la criptografía.
* Cantidad de asistentes: 15 personas

### Privacidad y anonimato para un manejo seguro de mi información en redes

- Ponentes:
  * [Diego Alberto Barriga Martínez]
  * [Gunnar Eyal Wolf Iszaevich]
  * [Ma. de Lourdes Reséndiz Martínez]
  * [Marco Ruano]
  * [Oscar Emilio Cabrera López]
- Fecha: 17 al 21 de Junio de 2019, 14:00 - 16:00
- Lugar: Facultad de Ciencias Políticas y Sociales, UNAM
- Descripción: El participante identificará las características asociadas a la
privacidad y el anonimato de la información a partir de los usos habituales de
los recursos digitales y utilizará algunas herramientas para promover la
protección y uso seguro de su información.
* Cantidad de asistentes: 15 personas

## Cursos planeados para el periodo intersemestral 2020-1 a 2020-2

### Python: Zero to Hero

* Ponentes:
  * [Diego Alberto Barriga Martínez]
* Fecha: 6 al 10 de Enero de 2020, 10:00 - 12:00
* Descripción: Por definir
* Asistentes esperados: 20 personas

### Privacidad y anonimato para un manejo seguro de mi información en redes

- Ponentes:
  * [Diego Alberto Barriga Martínez]
  * [Gunnar Eyal Wolf Iszaevich]
  * [Ma. de Lourdes Reséndiz Martínez]
  * [Oscar Emilio Cabrera López]
- Fecha: Por definir
- Lugar: Facultad de Ciencias Políticas y Sociales, UNAM
- Descripción: El participante identificará las características asociadas a la
privacidad y el anonimato de la información a partir de los usos habituales de
los recursos digitales y utilizará algunas herramientas para promover la
protección y uso seguro de su información.
* Asistentes esperados: 20 personas

### Sistema Operativo Android

* Ponentes:
  * [Oscar Emilio Cabrera López]
* Fecha: 6 al 17 de Enero de 2020, 10:00 - 12:00
* Descripción: Curso de Introducción al sistema operativo Android, que partes
lo componen, como funciona y como puede ser modificado. Este curso no es de
desarrollo de aplicaciones, sino de desarrollo del sistema operativo.
* Asistentes esperados: 15 personas

### Curso de Pandoc

* Ponentes:
  * [Oscar Emilio Cabrera López]
* Fecha: 6 al 10 de Enero de 2020, 12:00 - 14:00
* Descripción: En este curso aprenderemos a crear documentos de LATEX para
tesis, papers, libros, documentos PDF, presentaciones, páginas Web o libros
completos usando una sola herramienta de forma rápida y sencilla.
* Asistentes esperados: 15 personas

### Introducción a las redes neuronales: un enfoque interpretativo

* Ponentes:
  * Victor Mijangos
* Fecha: 20 al 24 de Enero de 2020, 12:00 - 14:00
* Descripción: Por definir
* Asistentes esperados: 15 personas


## Eventos organizados por LIDSoL

### FLISoL 2019

* Descripción: El Festival Latinoamericano de Instalación de Software Libre
(FLISoL), es el mayor evento de difusión del Software libre que se realiza
desde el año 2005 en diferentes países de manera simultánea.
* Fecha: 25 y 26 de Abril 2019
* Lugar: Auditorio Javier Barros Sierra
* Actividades:
  * Hackers & el Software Libre, el sistema inmunológico del internet
    * Ponente: Hiram Camarillo
    * Fecha: 25 de Abril, 11:00 - 12:00
    * Tipo: Charla
  * El proyecto Debian: Más allá de la madre de todas las distribuciones
    * Ponente: [Gunnar Eyal Wolf Iszaevich]
    * Fecha: 25 de Abril 2019, 12:00 - 13:00
    * Tipo: Charla
  * Ventajas de las comunidades y como contribuir
    * Ponente: Luis E. Jiménez Robles
    * Fecha: 25 de Abril 2019, 13:00 - 14:00
    * Tipo: Charla
  * DeepDream con TensorFlow
    * Ponente: Alejandro Hernández
    * Fecha: 25 de Abril 2019, 14:00 - 15:00
    * Tipo: Charla
  * José Alfredo Jiménez y Linux, sistemas operativos en tiempo real
    * Ponente: José María Serralde Ruiz
    * Fecha: 25 de Abril 2019, 15:00 - 16:00
    * Tipo: Charla
  * Protocolo ModBus: un riesgo presente en los sistemas de control industrial
    * Ponente: Paulo Contreras Flores
    * Fecha: 25 de Abril 2019, 16:00 - 17:00
  * No es tu amigo, es software privativo
    * Ponente: [Paul Sebastian Aguilar Enriquez]
    * Fecha: 26 de Abril 2019, 11:00 - 12:00
    * Tipo: Charla
  * Introducción a Fedora
    * Ponente: Efrén A. Robledo
    * Fecha: 26 de Abril 2019, 12:00 - 13:30
    * Tipo: Charla
  * Fedora Containers Lab: contenedores sin Docker
    * Ponente: Alex Callejas
    * 26 de Abril 2019, 13:30 - 15:00
    * Tipo: Charla
  * Se buscan personas programadoras: Cómo salvar al mundo sin morir en el intento
    * Ponente: Irene Soria
    * Fecha 26 de Abril 2019, 16:00 - 17:00
    * Tipo: Charla
  * Installfest
    * Descripción: Instalación de software libre en la computadora de los
      asistentes durante la duración del evento.

## Eventos en los que LIDSoL participó

### CCOSS

La Cumbre de Contribuidores de Open Source Software es un evento que se enfoca
en resolver el problema de las contribuciones a proyectos open source. A pesar
de que el uso de open source es basto en América Latina, las contribuciones son
mínimas. Para ello, se han dado a la tarea de juntar a una gran cantidad de
comunidades de open source y software libre que se encuentran alrededor del
país.

El laboratorio participó dando 3 charlas el primer día del evento. Los
organizadores cubrieron los gastos de transporte de los 5 integrantes del
laboratorio que asistieron al evento.

* Charla: Caso de Estudio Local: LIDSoL
  * Ponente: [Gunnar Eyal Wolf Iszaevich]
  * Fecha: 14 de Septiembre 2019, 11:20
  * Lugar: Instalaciones de Wizeline, Guadalajara, Jalisco
  * Descripción: El Laboratorio de Investigación y Desarrollo de Software Libre
(LIDSoL) es un laboratorio de la Facultad de Ingeniería de la UNAM, establecido
en 2001, con el objetivo de promover e impulsar la investigación y desarrollo
de tecnologías libres. En esta plática darmos a conocer algunos de los retos
y logros que hemos enfrentado, así como tips para replicar iniciativas
similares en otras instituciones educativas.
* Charla: Debian: El sistema operativo universal
  * Ponente: [Gunnar Eyal Wolf Iszaevich]
  * Fecha: 14 de Septiembre 2019, 14:30
  * Lugar: Instalaciones de Wizeline, Guadalajara, Jalisco
  * Descripción: La distribución Debian es mundialmente conocida como "el
Sistema Operativo Universal". Es una de las pocas distribuciones creada
exclusivamente por una comunidad de voluntarios de todo el mundo, sin
afiliación específica con una empresa que la dirija, y eso nos permite cubrir
básicamente los gustos y las necesidades de... Bueno, básicamente de cualquiera
que quiera partirse el lomo y colaborar con nosotros, y nos gustaría que uno
de estos voluntarios fueras tú.
* Charla: La red TOR
  * Ponentes:
    - [Diego Alberto Barriga Martínez]
    * Marco Ruano
    - [Oscar Emilio Cabrera López]
    - [Paul Sebastian Aguilar Enriquez]
  - Fecha: 14 de Septiembre 2019, 18:00
  * Lugar: Instalaciones de Wizeline, Guadalajara, Jalisco
  - Descripción: Creemos que cualquiera debe ser libre de explorar la red en
privacidad. En esta charla introducimos herramientas para proteger los derechos
y libertades digitales.

### Expo OS UPIITA

El grupo de OpenSource UPIITA organiza anualmente un evento de difusión de
software OpenSource. Este año el laboratorio fue invitado a participar en el
evento y se realizaron 1 taller y 2 charlas.

* Taller: Libera tu teléfono de las garras de Google
  - Ponente: [Oscar Emilio Cabrera López]
  - Fecha: 18 de Septiembre 2019, 10:00 - 11:30
  - Lugar: UPIITA-IPN
  - Descripción: Es este taller exploramos distintas técnicas para restringir el
acceso de Google a la información que almacenamos en nuestro teléfono, desde
desinstalar aplicaciones hasta eliminar por completo el sistema de fábrica y
reemplazarlo por uno que respete nuestra privacidad.
  * Asistentes: 12 personas
* Charla: Software Libre para sobrevivir a la universidad.
  * Ponente: [Diego Alberto Barriga Martínez]
  * Fecha: 18 de Septiembre 2019, 17:30
  - Lugar: UPIITA-IPN
  * Descripción: Poner a disposición del público estudiantil diferentes
herramientas de software libre, aplicadas a las ingenierías, como alternativas
para el desarrollo de trabajos escolares, proyectos y posible adopción para la
vida diaria.
  * Asistentes: 10 personas
* Charla: The Firefox Privacy Guide For Dummies!
  * Ponente: [Paul Sebastian Aguilar Enriquez]
  * Fecha: 18 de Septiembre 2019, 17:30
  - Lugar: UPIITA-IPN
  - Descripción: Proporcionar una guía sencilla (en la medida de lo posible)
sobre una configuración mejorada de la privacidad del navegador web Firefox
procurando rompe el menor número posible de sitios web.
  * Asistentes: 12 personas

### PythonDay MX 2019

* Taller: Cómo contribuir a CPython
  - Ponentes:
    * [Diego Alberto Barriga Martínez]
    * [Luis Alberto Oropeza Vilchis]
  - Fecha: 19/10/2019
  - Lugar: IIMAS, UNAM
  - Descripción: Python es un lenguaje ampliamente usado con una comunidad muy
activa. Si siempre has querido contribuir con el código fuente pero no sabes
por dónde empezar este taller es para ti. Desde la estructura del proyecto
hasta tu primera contribución

#  Necesidades

El Laboratorio necesita el apoyo de la división en varios aspectos.

## Papelería

- Plumones de pizarrón
- Toner para impresora

## Equipo de cómputo

Actualmente el laboratorio cuenta con dos computadoras con más de 10 años de
servicio que se usan como servidores, que no son suficientes para cubrir las
necesidades del laboratorio. Adicionalmente cuenta con dos equipos para uso de
los estudiantes cuyos periféricos comienzan a fallar.

- Actualización de 2 servidores
- 2 PC's (sin monitor) con teclado y mouse
- 2 Mouse
- 1 Regulador de corriente
- 2 Multicontactos
- 2 Extensiones

## Difusión de actividades

El laboratorio agradecería el apoyo en la difusión de los eventos que realiza de
la siguiente manera:

* Impresión de carteles para eventos
* Difusión de cursos, eventos y actividades en las redes sociales oficiales de
  la División
* Actualización de los datos de contacto del laboratorio en la pagina web de la
  División

